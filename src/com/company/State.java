package com.company;

public class State {
    private Node nodePlayer1;
    private Node nodePlayer2;

    public State(Node nodePlayer1, Node nodePlayer2) {
        this.nodePlayer1 = nodePlayer1;
        this.nodePlayer2 = nodePlayer2;
    }

    public Node getNodePlayer1() {
        return nodePlayer1;
    }

    public Node getNodePlayer2() {
        return nodePlayer2;
    }

    public void setState(Node node1, Node node2) {
        this.nodePlayer1 = node1;
        this.nodePlayer2 = node2;
    }
}
