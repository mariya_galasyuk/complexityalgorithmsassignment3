package com.company;

import java.util.ArrayList;

public class Node {
    private ArrayList <Move> moves = new ArrayList<>();
    private Color nodeColor;
    private int number;

    public Node(Color nodeColor, int number) {
        this.nodeColor = nodeColor;
        this.number = number;
    }

    public void addMove(Move move) {
        moves.add(move);
    }

    public int getNumber() {
        return number;
    }

    public Color getNodeColor() {
        return nodeColor;
    }

    public ArrayList<Move> getMoves() {
        return moves;
    }
}
