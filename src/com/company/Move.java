package com.company;

public class Move {
    private Node node;
    private Color arrowColor;

    public Move(Node node, Color arrowColor) {
        this.node = node;
        this.arrowColor = arrowColor;
    }

    public Color getArrowColor() {
        return arrowColor;
    }

    public Node getNode() {
        return node;
    }
}
