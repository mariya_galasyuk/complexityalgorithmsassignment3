package com.company;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

public class Game {
    private static Node node1 = new Node(Color.PINK, 1);
    private static Node node2 = new Node(Color.BLACK, 2);
    private static Node node3 = new Node(Color.GREEN, 3);
    private static Node node4 = new Node(Color.GREEN, 4);
    private static Node node5 = new Node(Color.GREEN, 5);
    private static Node node6 = new Node(Color.ORANGE, 6);
    private static Node node7 = new Node(Color.ORANGE, 7);
    private static Node node8 = new Node(Color.PINK, 8);
    private static Node node9 = new Node(Color.PINK, 9);
    private static Node node10 = new Node(Color.BLACK, 10);
    private static Node node11 = new Node(Color.ORANGE, 11);
    private static Node node12 = new Node(Color.PINK, 12);
    private static Node node13 = new Node(Color.ORANGE, 13);
    private static Node node14 = new Node(Color.GREEN, 14);
    private static Node node15 = new Node(Color.ORANGE, 15);
    private static Node node16 = new Node(Color.GREEN, 16);
    private static Node node17 = new Node(Color.GREEN, 17);
    private static Node node18 = new Node(Color.BLACK, 18);
    private static Node node19 = new Node(Color.ORANGE, 19);
    private static Node node20 = new Node(Color.GREEN, 20);
    private static Node node21 = new Node(Color.BLACK, 21);
    private static Node node22 = new Node(Color.BLACK, 22);
    private static Node node23 = new Node(Color.BLUE, 23);
    private static Move move1 = new Move(node5, Color.BLACK);
    private static Move move2 = new Move(node4, Color.PINK);
    private static Move move3 = new Move(node6, Color.GREEN);
    private static Move move4 = new Move(node12, Color.PINK);
    private static Move move5 = new Move(node1, Color.ORANGE);
    private static Move move6 = new Move(node4, Color.ORANGE);
    private static Move move7 = new Move(node13, Color.BLACK);
    private static Move move8 = new Move(node9, Color.ORANGE);
    private static Move move9 = new Move(node9, Color.GREEN);
    private static Move move10 = new Move(node10, Color.PINK);
    private static Move move11 = new Move(node2, Color.GREEN);
    private static Move move12 = new Move(node3, Color.PINK);
    private static Move move13 = new Move(node4, Color.GREEN);
    private static Move move14 = new Move(node14, Color.BLACK);
    private static Move move15 = new Move(node15, Color.GREEN);
    private static Move move16 = new Move(node10, Color.PINK);
    private static Move move17 = new Move(node12, Color.GREEN);
    private static Move move36 = new Move(node7,Color.GREEN);
    private static Move move18 = new Move(node18, Color.GREEN);
    private static Move move19 = new Move(node8, Color.GREEN);
    private static Move move20 = new Move(node20, Color.ORANGE);
    private static Move move21 = new Move(node23, Color.GREEN);
    private static Move move22 = new Move(node23, Color.PINK);
    private static Move move23 = new Move(node22, Color.GREEN);
    private static Move move24 = new Move(node15, Color.GREEN);
    private static Move move25 = new Move(node16, Color.BLACK);
    private static Move move26 = new Move(node11, Color.BLACK);
    private static Move move27 = new Move(node12, Color.PINK);
    private static Move move28 = new Move(node9, Color.ORANGE);
    private static Move move29 = new Move(node20, Color.ORANGE);
    private static Move move30 = new Move(node18, Color.GREEN);
    private static Move move31 = new Move(node19, Color.BLACK);
    private static Move move32 = new Move(node21, Color.ORANGE);
    private static Move move33 = new Move(node22, Color.ORANGE);
    private static Move move34 = new Move(node23, Color.BLACK);
    private static Move move35 = new Move(node17, Color.ORANGE);
    private static State start1 = new State(node1, node2);

    public void start() {
        addMoves();
        ArrayList<State> visited = new ArrayList<>();
        for (State state : dfs(start1, visited)){
            System.out.println(state);
        }
    }
    public boolean isGoalState(State state) {
        if(state.getNodePlayer1().getNumber()==23 || state.getNodePlayer2().getNumber()==23){
            return true;
        }else {
            return false;
        }


    }


    public void addMoves() {
        node1.addMove(move1);
        node1.addMove(move2);

        node2.addMove(move3);
        node2.addMove(move4);

        node3.addMove(move5);
        node3.addMove(move6);

        node4.addMove(move7);

        node5.addMove(move8);

        node6.addMove(move9);
        node6.addMove(move10);

        node7.addMove(move11);

        node8.addMove(move12);

        node9.addMove(move13);
        node9.addMove(move14);

        node10.addMove(move15);

        node11.addMove(move16);
        node11.addMove(move17);

        node12.addMove(move36);

        node13.addMove(move18);
        node13.addMove(move19);

        node14.addMove(move20);
        node14.addMove(move21);

        node15.addMove(move22);
        node15.addMove(move23);

        node16.addMove(move24);

        node17.addMove(move25);
        node17.addMove(move26);
        node17.addMove(move27);

        node18.addMove(move28);
        node18.addMove(move29);

        node19.addMove(move30);

        node20.addMove(move31);
        node20.addMove(move32);

        node21.addMove(move33);
        node21.addMove(move34);

        node22.addMove(move35);
    }

    public LinkedList<State> dfs(State start, ArrayList<State> visited) {
        LinkedList<State> solution;
        visited.add(start);
        if (isGoalState(start)) {
            solution = new LinkedList<>();
            solution.add(start);
            return solution;
        } else {
            List<State> neighbours = getNeighbours(start);
            for (State neighbour : neighbours) {
                if (!visited.contains(neighbour)) {
                    solution = dfs(neighbour, visited);
                    if (goalIsReached(solution)) {
                        solution.addFirst(start);
                        return solution;
                    }
                }
            }
        }
        return new LinkedList<State>();
    }

    public List<State> getNeighbours(State state) {
        List<State> neighbours = new LinkedList<>();
        for (Move move : state.getNodePlayer2().getMoves()) {
            if (move.getArrowColor().equals(state.getNodePlayer1().getNodeColor())){
                State state1 = new State(state.getNodePlayer1(), move.getNode());
                neighbours.add(state1);
            }

        }
        for (Move move : state.getNodePlayer1().getMoves()) {
            if (move.getArrowColor().equals(state.getNodePlayer2().getNodeColor())){
                State state1 = new State(move.getNode(),state.getNodePlayer2());
                neighbours.add(state1);
            }

        }
        return neighbours;
    }

    public boolean goalIsReached(LinkedList<State> solution) {
        for (int i = 0; i < solution.size(); i++) {
            if (isGoalState(solution.get(i))){
                return true;
            }
        }

        return false;
    }
}